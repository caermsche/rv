sampleFromMaf <- function(n, maf){
  drawnSample <- sample(x=c(0,1,2), prob=c((1-maf)^2, 2*maf*(1-maf), maf^2), size=n, replace=TRUE)
  return(drawnSample)
}

drawGene <- function(mafVec, n){
  nSnp <- length(mafVec)
  gene <- as.data.frame(do.call(cbind, lapply(mafVec, sampleFromMaf, n=n)))
  names(gene) <- paste("SNP", 1:nSnp, sep="")
  return(gene)
}

adaptBeta <- function(x, beta){
  if(x>25){beta_bmi  <- beta}
  else{beta_bmi <- 0}
  return(beta_bmi)
}

drawModel <- function(beta0=120, betaRare=8, betaCommon, errSd, n, mafVecRare, mafVecCommon, beta_smoke=5, beta_bmi=0.3, prev_smoke=0.3, bmiMean=25, bmiSd=2){
  
  mafVec <- c(mafVecRare, mafVecCommon)
  gene <- drawGene(mafVec, n=n)
  while(suppressWarnings(any(colSums(gene))==0)){
    gene <- drawGene(mafVec, n=n)
  }
  geneRare <- gene[, 1:length(mafVecRare)]
  geneCommon <- gene[, ((length(mafVecRare)+1):length(mafVec))]
  error <- rnorm(mean=0,sd=errSd,n=n)

  smoke <- sample(x=c(0,1), prob=c(prev_smoke, 1-prev_smoke), size=n, replace=TRUE)
  bmi <- rnorm(mean=bmiMean, sd=bmiSd, n=n)
  beta_bmi_vec <- sapply(bmi, adaptBeta, beta=beta_bmi)

  quanPhenoMod <- beta0 + betaRare*rowSums(geneRare) + as.matrix(geneCommon) %*% betaCommon + beta_smoke*smoke + beta_bmi_vec*bmi+ error

  covar <- data.frame(cov1=smoke, cov2=bmi)

  nCases <- ceiling(n/2)
  nControls <- n-nCases
  binPheno <-  rep(0, nCases+nControls)
  phenoRanks <- rank(quanPhenoMod)
  casesIdx <- which(phenoRanks %in% (length(binPheno)-nCases+1:length(binPheno)))
  controlsIdx <- sample(setdiff((1:length(binPheno)), casesIdx), size=nControls)
  binPheno[casesIdx] <- 1
  
  return(list(phenoQuan=as.vector(quanPhenoMod), phenoBin=as.vector(binPheno), roiGeno=as.matrix(gene), cov=as.matrix(covar)))
}

data <- drawModel(beta0=120,
                  betaRare=4,
                  betaCommon=c(sample(seq(from=0, to=0.05, by=0.003), replace=TRUE,size=15), sample(seq(from=0.05, to=1.2, by=0.1), replace=TRUE,size=5)),
                  errSd=9,
                  n=200,
                  mafVecRare=c(0.0055, 0.00591, 0.02, 0.025, 0.045),
                  mafVecCommon=c(sample(seq(from=0.1, to=0.5, by=0.01), replace=TRUE,size=15), sample(seq(from=0.05, to=0.1, by=0.051), replace=TRUE, size=5))
                  )

rvTestsExample <- data
colSums(rvTestsExample$roiGeno)
colSums(rvTestsExample$roiGeno[rvTestsExample$phenoBin==1, ])
str(rvTestsExample)
exampleFile <- "/home/dering/myWork/rv-tests/pkg/data/rvTestsExample.rda"
save(rvTestsExample, file=exampleFile)

# drawCaseCon <- function(nCases, nControls, data){
#   pheno <- data$phenoQuan
#   phenoRanks <- rank(pheno)
#   casesIdx <- which(phenoRanks %in% (length(pheno)-nCases+1:length(pheno)))
#   controlsIdx <- sample(setdiff((1:length(pheno)), casesIdx), size=nControls)
#   casesGeno <- data$geno[casesIdx, ]
#   casesCovar <- data$covar[casesIdx, ]
#   
#   controlsGeno <- data$geno[controlsIdx, ]
#   controlsCovar <- data$covar[controlsIdx, ]
# 
#   casConGeno <- rbind(casesGeno, controlsGeno)
#   casConCovar <- rbind(casesCovar, controlsCovar)
#   casConPheno <- c(rep(1, nCases), rep(0, nControls))
# 
#   casConData <- list(geno=casConGeno, covar=casConCovar, pheno=casConPheno)
#   return(casConData)
# }
# 
# casConData <- drawCaseCon(nCases=5000, nControls=5000, data=data)



