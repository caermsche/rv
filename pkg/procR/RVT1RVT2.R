
QuTie.dir <- file.path("/home/dering/myWork/dbGaP/R_perl_etc/QuTie-0.2.pl")
CCRaVaT.dir <- file.path("/home/dering/myWork/dbGaP/R_perl_etc/CCRaVAT-0.2.pl")

runRVT2bt <- function(maf,
                      pedDir,
                      replicateNr,
                      group,
                      seed,
                      covar,
                      pheno){
  extrSnpFile <- file.path(procDir,paste("RVT2_", group, "_extrSnps_maf_",maf, ".txt", sep=""))
  geneMapFile <- file.path(procDir,paste("RVT2_", group,"_geneMap_maf_",maf,".txt", sep=""))
  pedMapFileMafRoi <- paste(pedDir, "maf_", maf, "_roi_", group, sep="")
  pedFileMafRoiRepl <- paste(pedDir,"maf_",maf,"_roi_",group,"_replicate_",replicateNr,sep="")
  pedFile <- paste(pedFileMafRoiRepl, "ped", sep=".")
  mapFile <- paste(pedMapFileMafRoi, "map", sep=".")

  if(!(file.exists(paste(pedMapFileMafRoi, "ped",sep=".")))){
    plinkCmd <- paste("plink --noweb --file ", pedDir," --extract ",  extrSnpFile, " --recode --out ", pedMapFileMafRoi)
    system(plinkCmd)
  }
  extrSnp <- read.table(extrSnpFile, as.is=TRUE,stringsAsFactors=FALSE,header=FALSE)$V1
  col.classes <- c(rep(NULL, 6), rep("character", length(extrSnp)))
  ped <- read.table(paste(pedMapFileMafRoi,"ped",sep="."),as.is=TRUE,stringsAsFactors=FALSE,header=FALSE, colClasses=col.classes)
  rownames(ped) <- ped$V1
  unr_phenTab <- unr_phenList[[replicateNr]]
  rownames(unr_phenTab) <- unr_phenTab$ID
  unr_phenTab <- unr_phenTab[row.names(ped), ]
      
  outDir <- file.path(procDir, "RVT2_result")
  
 
  if(pheno=="binPhenotype"){
    ped$V6 <- unr_phenTab$AFFECTED
    write.table(ped, pedFile, quote=FALSE, row.names=FALSE, col.names=FALSE)

    cmd <- paste("perl", CCRaVaT.dir, "-gene -maf", maf, "-map", mapFile, "-ped", pedFile, "-cell 100 -nperm=100000 -pperm=0.00001 -genef", geneMapFile, "-outputf", outDir, "-plink",  sep=" ")
    outDir2 <- paste(procDir, "RVT2_result_CCRVgene_MAF_", maf, ".txt", sep="")
    system(cmd)
  resultTab <- read.delim(outDir2, skip=1, sep="\t", check.names=FALSE,header=TRUE, as.is=TRUE, stringsAsFactors=FALSE)
    pVal <- resultTab$FishExPval
  }
  else{
    ped$V6 <- unr_phenTab$Q2
    write.table(ped, pedFile, quote=FALSE, row.names=FALSE, col.names=FALSE)
    cmd <- paste("perl", QuTie.dir, "-gene -maf", maf, "-map", mapFile, "-ped", pedFile, "-nperm=100000 -pperm=0.00001 -genef", geneMapFile, "-outputf", outDir, "-plink",  sep=" ")
    outDir2 <- paste(outDir, "_QTRVgene_MAF_", maf, ".txt", sep="")
    system(cmd)
    resultTab <- read.delim(outDir2, skip=1, sep="\t", check.names=FALSE,header=TRUE, as.is=TRUE, stringsAsFactors=FALSE)
    pVal <- resultTab$pVal
  }
  system(cmd)
  resultTab <- read.delim(outDir2, skip=1, sep="\t", check.names=FALSE,header=TRUE, as.is=TRUE, stringsAsFactors=FALSE)

  resultNew <- data.frame(method="RVT2",
                          seed=seed,
                          perm=NA,
                          pheno=pheno,
                          gene=resultTab$Gene,
                          roi=group,
                          replicateNr=replicateNr,
                          covar=covar,
                          maf=maf,
                          nr_extr_T=NA,
                          asymp.pVal=pVal,
                          stringsAsFactors=FALSE)
  system(paste("rm ", pedFile, sep=""))
  return(resultNew)
}


maf=0.01
group="functional"
replicateNr=1
pedDir=file.path(procDir, "gaw17")
seed=2
covar=FALSE
pheno="quanPhenotype"
runRVT2bt(maf=maf, pedDir=pedDir, group=group, replicateNr=replicateNr, covar=covar, seed=seed,pheno=pheno)
unr_phenList[[1]]

## geneInfo <- read.table(geneInfoRVT2File, as.is=TRUE, sep="", stringsAsFactors=FALSE, header=TRU

## runRVT2qt <- function(maf, roi){
##   switch(roi,
##          damaged={
##            mapPed <- pedMapDamagedSLCO1B1
##          },
##          ns={
##            mapPed <- pedMapNonSynSLCO1B1
##          },
##          gene={
##            mapPed <- pedMapSLCO1B1
##          },
##          stop("You used a not predefined Region of interest! Analysis broke up.")
##          )
##   newMap <- paste(mapPed, "map", sep=".")
##   newPed <- paste(mapPed, "ped", sep=".")
##   outputf <- file.path(dirname(mapPed), "QuantitativeTrait", "RVT2", paste("SLCO1B1", roi,  sep="_"))
##   print(outputf)
##   cmd <- paste("perl", QuTie.dir, "-gene -maf", maf, "-map", newMap, "-ped", newPed, "-genef", geneInfoRVT2,"-outputf", outputf, "-plink",  sep=" ")
##   system(cmd)
## }

## runRVT2bt <- function(maf, roi, quantile){
##   switch(roi,
##          damaged={
##            mapPed <- pedMapDamagedSLCO1B1
##            map <- paste(pedMapDamagedSLCO1B1, "map", sep=".")
##            ped <- paste(pedMapDamagedSLCO1B1, "ped", sep=".")
##          },
##          ns={
##            mapPed <- pedMapNonSynSLCO1B1
##            map <- paste(pedMapNonSynSLCO1B1, "map", sep=".")
##            ped <- paste(pedMapNonSynSLCO1B1, "ped", sep=".")
##          },
##          gene={
##            mapPed <- pedMapSLCO1B1
##            map <- paste(pedMapSLCO1B1, "map", sep=".")
##            ped <- paste(pedMapSLCO1B1, "ped", sep=".")
##          },
##          stop("You used a not predefined Region of interest! Analysis broke up.")
##          )
##   sampleFile <- paste(dirname(map), "/BinaryTrait/RVT2/SampleOf_", quantile, "_quantile.txt", sep="")
##   newSampleFile <- paste(dirname(map), "/BinaryTrait/RVT2//PedSampleOf_", quantile, "_quantile.txt", sep="")
  
##   newFile <- paste(dirname(map), "/BinaryTrait/RVT2/", unlist(strsplit(basename(map), split="\\."))[1], "_", quantile, "_quantile", sep="")
##   newMapFile <- paste(newFile, ".map", sep="")
##   newPedFile <- paste(newFile, ".ped", sep="")
  
##   sample <- read.table(sampleFile, header=FALSE, as.is=FALSE)$V1
##   pedTab <- read.table(ped, header=FALSE, as.is=TRUE, stringsAsFactors=FALSE)
##   newSample <- subset(pedTab, V2 %in% sample, select=c(V1, V2))
##   write.table(newSample, newSampleFile, quote=FALSE, row.names=FALSE, col.names=FALSE)
##   outputf <- file.path(dirname(mapPed), "BinaryTrait", "RVT2", paste("SLCO1B1", roi,  "Quantil", quantile, sep="_"))
##   cmd <- paste("plink --file ", mapPed, " --keep ", newSampleFile, " --recode --out ", newFile, sep="")
##  system(cmd)
##   ## cmd <- paste("perl", CCRaVaT.dir, "-gene -maf", maf, "-map", newMapFile, "-ped", newPedFile, "-genef", geneInfoRVT2, "-outputf", outputf, "-plink",  sep=" ")
##   ## system(cmd)
## }


## runRVT1 <- function(maf, roi, quantile, method){
##   switch(roi,
##          damaged={
##            genFile <- paste(pedMapDamagedSLCO1B1, "gen", sep=".")
##            sampleFile <- paste(pedMapDamagedSLCO1B1, "sample", sep=".")
##          },
##          ns={
##            genFile <- paste(pedMapNonSynSLCO1B1, "gen", sep=".")
##            sampleFile <- paste(pedMapNonSynSLCO1B1, "sample", sep=".")
##          },
##          gene={
##            genFile <- paste(pedMapSLCO1B1, "gen", sep=".")
##            sampleFile <- paste(pedMapSLCO1B1, "sample", sep=".")
##          },
##          stop("You used a not predefined Region of interest! Analysis broke up.")
##          )
##   if(!(quantile %in% c(0, 1))){
##     sampleTable <- read.table(sampleFile, as.is=TRUE, header=TRUE,
##                               col.names=c("ID_1", "ID_2", "missing", "sex", "Phenotype", "PhenotypeQ"),
##                               skip=1)

##     orderSampleT <- sampleTable[order(sampleTable$PhenotypeQ), ]
##     row.names(orderSampleT) <- orderSampleT$ID_1
    
##     cases <- orderSampleT$ID_1[1:round(quantile*nrow(orderSampleT))]
##     controls <- orderSampleT$ID_1[round((1-quantile)*nrow(orderSampleT)):nrow(orderSampleT)]
##     extrSamplesFile <- paste(dirname(sampleFile), "/BinaryTrait/", method, "/SampleOf_", quantile, "_quantile.txt", sep="")
    
##     write.table(c(cases, controls), extrSamplesFile, sep= " ",  quote=FALSE, col.names=FALSE,
##                 row.names=FALSE)
##     outFile <- file.path(pedMapSLCO1B1.dir, paste("BinaryTrait/", method, "/SLCO1B1_", roi, "_QT_", method, "_Maf_",maf, "Quantile_",quantile, sep=""))
  
##   GranvilCmd <- paste(Granvil.dir, "-g", genFile, "-s", sampleFile, "--pheno Phenotype -r", maf, "--genmap", geneInfoRVT1, "--extract_samples", extrSamplesFile, "-m expected --out", outFile)
##     ## system(paste("cd ", dirname(outFile), " \n", "rm *.err \n rm *.log", sep=""))
##   }
##   else{
##     outFile <- file.path(pedMapSLCO1B1.dir, paste("QuantitativeTrait/", method, "/SLCO1B1_", roi, "_BT_", method, "_Maf_",maf, "Quantile_", quantile, sep=""))

##     GranvilCmd <- paste(Granvil.dir, "-g", genFile, "-s", sampleFile, "--pheno PhenotypeQ -r", maf, "--genmap", geneInfoRVT1, "-m expected --out", outFile)
##   }
##   system(GranvilCmd)
##   system(paste("cd ", dirname(outFile), " \n", "rm *.err \n rm *.log", sep=""))
## }





                             
