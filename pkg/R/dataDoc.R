#' Genotype, phenotype and covariate data in 10,000 individuals
#' 
#' \itemize{
#' \item 'phenoBin'. Case control data
#' \item 'phenoQuan'. Quantitative phenotype data.
#' \item 'roiGeno'. Data frame with 25 column-wise genotypes of 10,000 individuals in the rows.
#' \item 'cov'. Matrix containing one categorial and one continuous covariate for each individual.
#' }
#'  
#' @docType data
#' @keywords datasets
#' @name rvTestsExample
#' @usage data(rvTestsExample)
#' @format A list of length 4. 
#' 
NULL