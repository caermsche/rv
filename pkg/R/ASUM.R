##########################################################################
###  This script is based on the implementation of the
###  AssotestR-R-package, an implementation of Gaston Sanchez
###  Modifications were conducted by Carmen Dering
##########################################################################

ASUM <- function (y, X, perm = 100, maf=0.05){
    MAFs = colSums(X, na.rm = TRUE)/(2 * nrow(X))
    rare = sum(MAFs < maf)
  if (rare > 1) {
    X <- X[, MAFs < maf]
    getuv = my_getUV(y, X)
    U = getuv$U
    V = getuv$V
    stat.asum = my_asum_method(U, V)
    asum.stat1 = stat.asum[1]
    p1.asum = stat.asum[2]
    perm.pval = NA
    if (perm > 0) {
        p1.perm = rep(0, perm)
        ymean = mean(y)
        for (i in 1:perm) {
            perm.sample = sample(1:length(y))
            y.perm = y[perm.sample] - ymean
            U.perm = colSums(y.perm * X, na.rm = TRUE)
            perm.asum = my_asum_method(U.perm, V)
            p1.perm[i] = perm.asum[2]
        }
        sum.T.stat <- sum(p1.perm < p1.asum)
        perm.pval <- (sum(p1.perm < p1.asum)+1)/(perm + 1)
        asymp.pval <- NA
    }
  }
   else {
    print(paste("\n", "Oops: No rare variants below maf=", 
                maf, " were detected. Try a larger maf", sep = ""))
    sum.T.stat = NA
    asym.pval = NA
    perm.pval=NA
  }  
    res <- list(nrExtrT=sum.T.stat, permPVal=perm.pval, asymPVal=asymp.pval)
    return(res)
}


my_asum_method <- function (U, V){
    Ords = order(U/sqrt(diag(V)), decreasing = TRUE)
    Uords = U[Ords]
    V.ords = V[Ords, Ords]
    k = length(U)
    scores <- scores.ord <- rep(0, k)
    pvals <- pvals.ord <- rep(0, k)
    for (j in 1:k) {
        aux = my_sum_method(U[1:j], V[1:j, 1:j])
        scores[j] = aux[1]
        pvals[j] = aux[2]
        aux.ord = my_sum_method(Uords[1:j], V.ords[1:j, 1:j])
        scores.ord[j] = aux.ord[1]
        pvals.ord[j] = aux.ord[2]
    }
    p1 = min(pvals)
    p2 = min(pvals.ord)
    S1 = scores[pvals == p1]
    S2 = scores.ord[pvals.ord == p2]
    c(S1, p1, S2, p2)
}


my_getUV <- function (y, X) 
{
    y.new = y - mean(y)
    U = colSums(y.new * X, na.rm = TRUE)
    X.new = scale(X, scale = FALSE)
    if (sum(complete.cases(X)) != length(y)) {
        tX.new = t(X.new)
        Sx = matrix(0, ncol(X), ncol(X))
        for (i in 1:nrow(tX.new)) {
            for (j in i:ncol(X.new)) {
                Sx[i, j] = sum(tX.new[i, ] * X.new[, j], na.rm = TRUE)
            }
        }
        sx.diag = diag(Sx)
        Sx = Sx + t(Sx)
        diag(Sx) = sx.diag
        V = mean(y) * (1 - mean(y)) * Sx
    }
    else {
        V = mean(y) * (1 - mean(y)) * (t(X.new) %*% X.new)
    }
    res.uv = list(U = U, V = V)
    return(res.uv)
}


 my_sum_method <- function (U, V) 
{
    if (abs(sum(U)) < 1e-20) {
        score = 0
        pval = 1
    }
    else {
        ones = rep(1, length(U))
        score = sum(U)/sqrt(sum(V))
        pval = 1 - pchisq(score^2, 1)
    }
    res = c(score, pval)
    res
}

## my_check <- function (y, X, perm){
##     if (!is.vector(y) || mode(y) != "numeric") 
##         stop("Sorry, argument 'y' must be a numeric vector")
##     if (any(is.na(y))) 
##         stop("No missing data allowed in argument 'y' ")
##     if (!all(y %in% c(0, 1))) 
##         stop("Argument 'y' must contain only 0 and 1")
##     if (!is.matrix(X) & !is.data.frame(X)) 
##         stop("Argument 'X' must be a matrix or data.frame")
##     if (nrow(X) != length(y)) 
##         stop("'X' and 'y' have different lengths")
##     if (!is.matrix(X)) 
##         X = as.matrix(X)
##     if (mode(perm) != "numeric" || length(perm) != 1 || perm < 
##         0 || (perm%%1) != 0) {
##         warning("argument 'perm' incorrectly defined. Value perm=100 is used")
##         perm = 100
##     }
##     list(y = y, X = X, perm = perm)
## }
