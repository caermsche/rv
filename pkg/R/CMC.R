##########################################################################
###  This script is based on the implementation of the
###  AssotestR-R-package, an implementation of Gaston Sanchez
###  Modifications were conducted by Carmen Dering
##########################################################################

CMC <- function (y, X, maf = 0.05, perm = NULL){
  N = nrow(X)
  MAF = colMeans(X, na.rm = TRUE)/2
  
  rare.maf = MAF < maf
  rare = sum(rare.maf)
  
  if(!(rare < 2)){
    if(rare == dim(X)[2]){
      X.collaps = rowSums(X[, rare.maf], na.rm = TRUE)
      X.collaps[X.collaps != 0] = 1
      props = table(y, X.collaps)
      stat = unlist(fisher.test(props)[c(3, 1)])
      names(stat) = NULL
      cmc.stat = stat[1]
      asym.pval = stat[2]
    }
    else{
      ## rare != dim(X)[2]
      X.collaps = rowSums(X[, rare.maf], na.rm = TRUE)
      X.collaps[X.collaps != 0] = 1
      X.new = cbind(X[, !rare.maf], X.collaps)
      
      X.new = X.new - 1
      M = ncol(X.new)
      cmc.stat = my_cmc_method(y, X.new)
      f.stat = cmc.stat * (N - M - 1)/(M * (N - 2))
      df1 = M
      df2 = N - M - 1
      
      asym.pval = pf(f.stat, df1, df2, lower.tail=FALSE)
    }
  }
  else {
    print(paste("\n", "Oops: No rare variants below maf=", 
                maf, " were detected. Try a larger maf", sep = ""))
    cmc.stat <- "none"
    asym.pval <- NA
    ## X.new = X
  }
  perm.pval = NA
  if (!(is.null(perm))) {
    x.perm = rep(0, perm)
    for (i in 1:perm) {
      perm.sample = sample(1:length(y))
      x.perm[i] = my_cmc_method(y[perm.sample], X.new)
    }
    sum.T.stat = sum(x.perm > cmc.stat)
    perm.pval = (sum(x.perm > cmc.stat)+1)/(perm+1)
  }
  else{
    sum.T.stat = NA
    perm.pval = NA
  }
  res <- list(nrExtrT=sum.T.stat, permPVal=perm.pval, asymPVal=asym.pval)
  return(res)
}


my_cmc_method <- function (casecon, X.new) {  
  N = nrow(X.new)
  nA = sum(casecon)
  nU = N - nA
  Xx = X.new[casecon == 1, ]
  Yy = X.new[casecon == 0, ]
  Xx.mean = colMeans(Xx, na.rm = TRUE)
  Yy.mean = colMeans(Yy, na.rm = TRUE)
  Dx = sweep(Xx, 2, Xx.mean)
  Dy = sweep(Yy, 2, Yy.mean)
  if (sum(complete.cases(X.new)) == N) {
    COV = (t(Dx) %*% Dx + t(Dy) %*% Dy)/(N - 2)
  }
  else {
    tDx = t(Dx)
        Sx = matrix(0, ncol(X.new), ncol(X.new))
        for (i in 1:nrow(tDx)) {
            for (j in i:ncol(Dx)) {
                Sx[i, j] = sum(tDx[i, ] * Dx[, j], na.rm = TRUE)
            }
        }
        sx.diag = diag(Sx)
        Sx = Sx + t(Sx)
        diag(Sx) = sx.diag
        tDy = t(Dy)
        Sy = matrix(0, ncol(X.new), ncol(X.new))
        for (i in 1:nrow(tDy)) {
            for (j in i:ncol(Dy)) {
                Sy[i, j] = sum(tDy[i, ] * Dy[, j], na.rm = TRUE)
            }
        }
        sy.diag = diag(Sy)
        Sy = Sy + t(Sy)
        diag(Sy) = sy.diag
        COV = (1/(N - 2)) * (Sx + Sy)
    }
    if (nrow(COV) == 1) {
        if (COV < 1e-08) 
            COV = 1e-08
        COV.inv = 1/COV
    }
    else {
        COV.eigen = eigen(COV)
        eig.vals = COV.eigen$values
        inv.vals = ifelse(abs(eig.vals) <= 1e-08, 0, 1/eig.vals)
        EV = solve(COV.eigen$vectors)
        COV.inv = t(EV) %*% diag(inv.vals) %*% EV
    }
    stat = t(Xx.mean - Yy.mean) %*% COV.inv %*% (Xx.mean - Yy.mean) * 
        nA * nU/N
    
    as.numeric(stat)
}
## my_check <- function (y, X, perm){
##     if (!is.vector(y) || mode(y) != "numeric") 
##         stop("Sorry, argument 'y' must be a numeric vector")
##     if (any(is.na(y))) 
##         stop("No missing data allowed in argument 'y' ")
##     if (!all(y %in% c(0, 1))) 
##         stop("Argument 'y' must contain only 0 and 1")
##     if (!is.matrix(X) & !is.data.frame(X)) 
##         stop("Argument 'X' must be a matrix or data.frame")
##     if (nrow(X) != length(y)) 
##         stop("'X' and 'y' have different lengths")
##     if (!is.matrix(X)) 
##         X = as.matrix(X)
##     if (mode(perm) != "numeric" || length(perm) != 1 || perm < 
##         0 || (perm%%1) != 0) {
##         warning("argument 'perm' incorrectly defined. Value perm=100 is used")
##         perm = 100
##     }
##     list(y = y, X = X, perm = perm)
## }
