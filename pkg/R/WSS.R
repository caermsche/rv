##########################################################################
###
###  This script is an original implementation of Carmen Dering
###  
##########################################################################

weights <-  function(matrix, NrControls, NrInd){
  ratio <-colSums(matrix)
  ##     ratioNew <- unlist(ratio)
  q <- (ratio + 1)/(2*(NrControls + 1))
  weight <- sqrt(NrInd*q*(1 - q))
  return(weight)
}

gamma_calc <- function(dataframe, w_vec){
  rezi_w_vec <- (w_vec)^(-1)
  matrix <- as.matrix(dataframe)
  gamma <- matrix %*% rezi_w_vec
  return(gamma)
}

ranksum <- function(vector, indicator){
  ##   indicator <- unlist(indicator)
  scalarproduct <- vector %*% indicator
  return(scalarproduct)
}

phenoAssignment <- function(indices, object){
  newObject <- object[indices,]
  return(newObject)
}

ranksumCalc <- function(pheno, gene){
  indices_controls <- which(pheno  == 0)
  indices_cases <- which(pheno  == 1)
  n_controls <- length(indices_controls)
  n_cases <- length(indices_cases)
  n <- n_controls + n_cases
  GeneControls <- phenoAssignment(gene, indices = indices_controls)
  
  w_vec <- weights(GeneControls,NrControls = n_controls, NrInd = n)
  gamma <- gamma_calc(gene, w_vec = w_vec)
  
  ranks <- rank(gamma)
  ranksum_cases <- ranksum(ranks, indicator = pheno)
  return(ranksum_cases)
}

WSS_classic <- function(phenotype,
                        genotype,
                        perm,
                        maf){
  MAFs <- colMeans(genotype, na.rm = TRUE)/2
  rare = sum(MAFs < maf)
  if (rare <= 1) 
    stop(paste("Ooops!  No rare variants detected below maf=", 
               maf, sep = ""))
  else {

    genotype <- genotype[, MAFs < maf]
    n_perm <- perm
    phenoOriginal <- phenotype
    if (!(is.null(perm))) {
      ranksum_permutation <- rep(0, n_perm)
      for(i in 1:n_perm){
        perm_sample <- sample(1:length(phenoOriginal))
        pheno_Sample <- phenoOriginal[perm_sample]
        ranksum_permutation[i] <- ranksumCalc(pheno_Sample, gene = genotype)
      }
    ranksum_original <- ranksumCalc(phenoOriginal, genotype)
    sum.T.stat <- sum(ranksum_permutation > as.vector(ranksum_original))
    perm.pval <- (sum.T.stat + 1)/(n_perm+1)
    }
    else{
      sum.T.stat <- NA
      perm.pval <- NA
    }
    asym.pval <- WSS_normal(phenotype, genotype, perm=1000, maf=maf)
    res <- list(nrExtrT=sum.T.stat, permPVal=perm.pval, asymPVal=asym.pval)
    return(res)
  }
}

WSS_normal <- function(phenotype,
                       genotype,
                       perm,
                       ## resultDir,
                       maf){
  ## MAFs <- colMeans(genotype, na.rm = TRUE)/2
  ## genotype <- genotype[, MAFs < maf]
  n_perm <- perm
  phenoOriginal <- phenotype
  ranksum_permutation <- rep(0, n_perm)
  for(i in 1:n_perm){
    perm_sample <- sample(1:length(phenoOriginal))
    pheno_Sample <- phenoOriginal[perm_sample]
    ranksum_permutation[i] <- ranksumCalc(pheno_Sample, gene = genotype)
  }
  ranksum_original <- ranksumCalc(phenoOriginal, genotype)
  mean_perm <- mean(ranksum_permutation)
  sd_perm <- sd(ranksum_permutation)
  z <- (ranksum_original - mean_perm)/sd_perm
  p_value <- 2 * pnorm(abs(z), lower.tail = FALSE)[1,1]
  return(p_value)
}

