##########################################################################
###  This scriptis is based on the implementation of the
###  AssotestR-R-package, an implementation of Gaston Sanchez
###  Modifications were conducted by Carmen Dering
##########################################################################

CALPHA <- function (y, X, perm = 100, maf) {
  MAFs = colMeans(X, na.rm = TRUE)/2
  rare = sum(MAFs < maf)
  if (rare > 1) {
    nA = sum(y)
    nU = sum(y == 0)
    p0 = nA/(nA + nU)
    m = ncol(X)
    n = apply(X, 2, function(x) sum(x > 0, na.rm = TRUE))
    g = apply(X[y == 1, ], 2, function(x) sum(x > 0, na.rm = TRUE))
    
    calpha.stat = my_calpha_method(y, X)
    Valpha = 0
    for (i in 1:m) {
      for (u in 0:n[i]) {
        Valpha = Valpha + (((u - n[i] * p0)^2 - n[i] * p0 * 
          (1 - p0))^2) * dbinom(u, n[i], p0)
      }
    }
    names(Valpha) = NULL
    Zscore <- calpha.stat/sqrt(Valpha)
    if (Valpha == 0) 
      asym.pval = 1
    else{
      asym.pval = pnorm(Zscore, lower.tail=FALSE)
    }
    perm.pval = NA
    if (!(is.null(perm))) {
      x.perm = rep(0, perm)
      for (i in 1:perm) {
        perm.sample = sample(1:length(y))
        x.perm[i] = my_calpha_method(y[perm.sample], X)
      }
      sum.T.stat <- sum(x.perm^2 > calpha.stat^2) 
      perm.pval = (sum.T.stat + 1)/(perm+1)
    }
    else{
      sum.T.stat = NA
      perm.pval <- NA
    }
  }
  else {
    print(paste("\n", "Oops: No rare variants below maf=", 
                maf, " were detected. Try a larger maf", sep = ""))
    sum.T.stat = NA
    asym.pval = NA
    perm.pval=NA
  }
  res <- list(nrExtrT=sum.T.stat, permPVal=perm.pval, asymPVal=asym.pval)
  return(res)
}



my_calpha_method <- function (casecon, gen) 
{
    nA = sum(casecon)
    nU = sum(casecon == 0)
    p0 = nA/(nA + nU)
    m = ncol(gen)
    n = apply(gen, 2, function(x) sum(x > 0, na.rm = TRUE))
    g = apply(gen[casecon == 1, ], 2, function(x) sum(x > 0, 
        na.rm = TRUE))
    Talpha = sum((g - n * p0)^2 - (n * p0 * (1 - p0)))
    Talpha
}

## my_check <- function (y, X, perm){
##     if (!is.vector(y) || mode(y) != "numeric") {
##         stop("Sorry, argument 'y' must be a numeric vector")
##     }
##     if (any(is.na(y))) 
##         stop("No missing data allowed in argument 'y' ")
##     if (!all(y %in% c(0, 1))) 
##         stop("Argument 'y' must contain only 0 and 1")
##     if (!is.matrix(X) & !is.data.frame(X)) 
##         stop("Argument 'X' must be a matrix or data.frame")
##     if (nrow(X) != length(y)) 
##         stop("'X' and 'y' have different lengths")
##     if (!is.matrix(X)) 
##         X = as.matrix(X)
##     if (mode(perm) != "numeric" || length(perm) != 1 || perm < 
##         0 || (perm%%1) != 0) {
##         warning("argument 'perm' incorrectly defined. Value perm=100 is used")
##         perm = 100
##     }
##     list(y = y, X = X, perm = perm)
## }
