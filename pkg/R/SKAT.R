##########################################################################
###  This script is based on the R-package SKAT.R. Its purpose is to
###  reduce preprocessing to get appropriate input objects (according
###  to available phenotype and covariates) to the SKAT-function and
###  to also produce a p-value from permutation. Modifications were
###  conducted by Carmen Dering
##########################################################################

SKAT_wrap <- function(covarMat, phenotype, genotype, kernel, weights, outType, perm){
  if(!(is.null(covarMat))){
    dimnames(covarMat) <- NULL
    obj <- SKAT::SKAT_Null_Model(phenotype ~ covarMat, out_type=outType, Adjustment=TRUE)
  }
  else{
    obj <- SKAT::SKAT_Null_Model(phenotype ~ 1, out_type=outType, Adjustment=TRUE)
  }
  print(weights)
  SKAT_Res <- SKAT::SKAT(as.matrix(genotype),obj, kernel=kernel,weights.beta=weights)
  asym.pval <- SKAT_Res$p.value
  skat.stat <- SKAT_Res$Q[1,1]

  if((is.null(perm))){
    sum.T.stat = NA
    permPVal=NA
  }
  else{
    x.perm = rep(0, perm)
    for (i in 1:perm) {
      perm.sample = sample(1:length(phenotype))
      if(!(is.null(covarMat))){
        dimnames(covarMat) <- NULL
        obj <- SKAT::SKAT_Null_Model(phenotype[perm.sample] ~ covarMat, out_type=outType, Adjustment=TRUE)
      }
      else{
        obj <- SKAT::SKAT_Null_Model(phenotype[perm.sample] ~ 1, out_type=outType, Adjustment=TRUE)
      }
      SKAT_PermRes <- SKAT::SKAT(as.matrix(genotype),obj, kernel=kernel,weights.beta=weights)
      x.perm[i] <- SKAT_PermRes$Q
    }
    sum.T.stat <- sum(x.perm > skat.stat) 
    permPVal = (sum.T.stat + 1)/(perm+1)
  }
  res <- list(nrExtrT=sum.T.stat, permPVal=permPVal, asymPVal=asym.pval)
  return(res)
}


