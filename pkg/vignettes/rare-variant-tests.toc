\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Laden des Pakets}{2}{subsection.1.1}
\contentsline {section}{\numberline {2}Beispieldatensatz}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}MAF-Struktur}{3}{subsection.2.1}
\contentsline {section}{\numberline {3}Die Hauptfunktion \texttt {evalCollapsing}}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Eingabe-Parameter}{3}{subsection.3.1}
\contentsline {subsection}{evalCollapsing}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Funktionsaufruf und Ausgabeformat}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}Gruppierungsmethoden}{5}{section.4}
\contentsline {subsection}{\numberline {4.1}CAST - Cohort Allelic Sum Test}{5}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Algorithmus}{6}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Verwendete Implementation}{6}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Verwendung anhand des Beispieldatensatzes}{7}{subsubsection.4.1.3}
\contentsline {subsection}{\numberline {4.2}CMC - Combined and Multivariate Collapsing}{7}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Algorithmus}{7}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Verwendete Implementation}{8}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Verwendung anhand des Beispieldatensatzes}{8}{subsubsection.4.2.3}
\contentsline {subsection}{\numberline {4.3}RC - Rarecover}{9}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Algorithmus}{9}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Verwendete Implementation}{10}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Verwendung anhand des Beispieldatensatzes}{10}{subsubsection.4.3.3}
\contentsline {subsection}{\numberline {4.4}RVT1 - Rare Variant Test 1}{10}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Algorithmus}{11}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Verwendete Implementation}{11}{subsubsection.4.4.2}
\contentsline {subsubsection}{\numberline {4.4.3}Verwendung anhand des Beispieldatensatzes}{12}{subsubsection.4.4.3}
\contentsline {subsection}{\numberline {4.5}RVT2 - Rare Variant Test 2}{13}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}Algorithmus}{14}{subsubsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.2}Verwendete Implementation}{14}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Verwendung anhand des Beispieldatensatzes}{14}{subsubsection.4.5.3}
\contentsline {subsection}{\numberline {4.6}WSS - Weighted Sum Statistic}{16}{subsection.4.6}
\contentsline {subsubsection}{\numberline {4.6.1}Algorithmus}{16}{subsubsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.2}Verwendete Implementation}{17}{subsubsection.4.6.2}
\contentsline {subsubsection}{\numberline {4.6.3}Verwendung anhand des Beispieldatensatzes}{17}{subsubsection.4.6.3}
\contentsline {subsection}{\numberline {4.7}CMAT - Cumulative Minor-Allele Test}{18}{subsection.4.7}
\contentsline {subsubsection}{\numberline {4.7.1}Algorithmus}{18}{subsubsection.4.7.1}
\contentsline {subsubsection}{\numberline {4.7.2}Verwendete Implementation}{19}{subsubsection.4.7.2}
\contentsline {subsubsection}{\numberline {4.7.3}Verwendung anhand des Beispieldatensatzes}{19}{subsubsection.4.7.3}
\contentsline {subsection}{\numberline {4.8}VT - Variable Threshold}{19}{subsection.4.8}
\contentsline {subsubsection}{\numberline {4.8.1}Algorithmus}{20}{subsubsection.4.8.1}
\contentsline {subsubsection}{\numberline {4.8.2}Verwendete Implementation}{21}{subsubsection.4.8.2}
\contentsline {subsubsection}{\numberline {4.8.3}Verwendung anhand des Beispieldatensatzes}{21}{subsubsection.4.8.3}
\contentsline {subsection}{\numberline {4.9}aSum - Adaptive Summation Test}{22}{subsection.4.9}
\contentsline {subsubsection}{\numberline {4.9.1}Algorithmus}{22}{subsubsection.4.9.1}
\contentsline {subsubsection}{\numberline {4.9.2}Verwendete Implementation}{23}{subsubsection.4.9.2}
\contentsline {subsubsection}{\numberline {4.9.3}Verwendung anhand des Beispieldatensatzes}{23}{subsubsection.4.9.3}
\contentsline {subsection}{\numberline {4.10}KBAC - Kernel Based Adaptive Cluster}{24}{subsection.4.10}
\contentsline {subsubsection}{\numberline {4.10.1}Algorithmus}{24}{subsubsection.4.10.1}
\contentsline {subsubsection}{\numberline {4.10.2}Verwendete Implementation}{25}{subsubsection.4.10.2}
\contentsline {subsubsection}{\numberline {4.10.3}Verwendung anhand des Beispieldatensatzes}{26}{subsubsection.4.10.3}
\contentsline {subsection}{\numberline {4.11}C-Alpha - C-Alpha-based Test}{26}{subsection.4.11}
\contentsline {subsubsection}{\numberline {4.11.1}Algorithmus}{27}{subsubsection.4.11.1}
\contentsline {subsubsection}{\numberline {4.11.2}Verwendete Implementation}{27}{subsubsection.4.11.2}
\contentsline {subsubsection}{\numberline {4.11.3}Verwendung anhand des Beispieldatensatzes}{27}{subsubsection.4.11.3}
\contentsline {subsection}{\numberline {4.12}FPCA - Functional Principal Component Analysis}{28}{subsection.4.12}
\contentsline {subsubsection}{\numberline {4.12.1}Algorithmus}{29}{subsubsection.4.12.1}
\contentsline {subsubsection}{\numberline {4.12.2}Verwendete Implementation}{30}{subsubsection.4.12.2}
\contentsline {subsubsection}{\numberline {4.12.3}Verwendung anhand des Beispieldatensatzes}{30}{subsubsection.4.12.3}
\contentsline {subsection}{\numberline {4.13}PWST - P-value Weighted Sum Test}{31}{subsection.4.13}
\contentsline {subsubsection}{\numberline {4.13.1}Algorithmus}{31}{subsubsection.4.13.1}
\contentsline {subsubsection}{\numberline {4.13.2}Verwendete Implementation}{32}{subsubsection.4.13.2}
\contentsline {subsubsection}{\numberline {4.13.3}Verwendung anhand des Beispieldatensatzes}{32}{subsubsection.4.13.3}
\contentsline {subsection}{\numberline {4.14}SKAT - Sequencing Kernel Association Test}{33}{subsection.4.14}
\contentsline {subsubsection}{\numberline {4.14.1}Algorithmus}{33}{subsubsection.4.14.1}
\contentsline {subsubsection}{\numberline {4.14.2}Verwendete Implementation}{35}{subsubsection.4.14.2}
\contentsline {subsubsection}{\numberline {4.14.3}Verwendung anhand des Beispieldatensatzes}{35}{subsubsection.4.14.3}
\contentsline {subsection}{\numberline {4.15}SKAT-O - Optimized SKAT}{38}{subsection.4.15}
\contentsline {subsubsection}{\numberline {4.15.1}Algorithmus}{38}{subsubsection.4.15.1}
\contentsline {subsubsection}{\numberline {4.15.2}Verwendete Implementation}{39}{subsubsection.4.15.2}
\contentsline {subsubsection}{\numberline {4.15.3}Verwendung anhand des Beispieldatensatzes}{39}{subsubsection.4.15.3}
